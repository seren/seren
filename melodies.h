/*
 * Copyright (C) 2013, 2014 Giorgio Vazzana
 *
 * This file is part of Seren.
 *
 * Seren is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Seren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MELODIES_H
#define MELODIES_H

#include "notes.h"

/* Avaiable songs:
 * 0 - inno_mameli
 * 1 - god_save_the_queen
 * 2 - ode_to_joy
 * 3 - nokia_ringtone
 * 4 - over_the_rainbow
 */
#define SONGNUM 2

struct note {
	double       freq;
	unsigned int duration; /* in milliseconds */
};

#if SONGNUM==0
#define SONG inno_mameli
#define D 600
/* http://www.radiomarconi.com/marconi/mameli_spartito.gif */
const struct note inno_mameli[] = {
	{NOTE_D4,  D},

	{NOTE_D4,  D/2+D/4},
	{NOTE_E4,  D/4},
	{NOTE_D4,  2*D},
	{NOTE_B4,  D},

	{NOTE_B4,  D/2+D/4},
	{NOTE_C5,  D/4},
	{NOTE_B4,  2*D},
	{NOTE_B4,  D},

	{NOTE_D5,  D/2+D/4},
	{NOTE_C5,  D/4},
	{NOTE_B4,  2*D},
	{NOTE_A4,  D},

	{NOTE_B4,  D/2+D/4},
	{NOTE_A4,  D/4},
	{NOTE_G4,  2*D},
	{NOTE_D4,  D},


	{NOTE_D4,  D/2+D/4},
	{NOTE_E4,  D/4},
	{NOTE_D4,  2*D},
	{NOTE_B4,  D},

	{NOTE_B4,  D/2+D/4},
	{NOTE_C5,  D/4},
	{NOTE_B4,  2*D},
	{NOTE_B4,  D},

	{NOTE_D5,  D/2+D/4},
	{NOTE_C5,  D/4},
	{NOTE_B4,  2*D},
	{NOTE_A4,  D},

	{NOTE_B4,  D/2+D/4},
	{NOTE_A4,  D/4},
	{NOTE_G4,  2*D},
	{NOTE_B4,  D},

	{NOTE_B4,  D},
	{NOTE_F4d, 2*D},
	{NOTE_G4,  D/2+D/4},
	{NOTE_A4,  D/4},


	{NOTE_G4,  D/2+D/4},
	{NOTE_F4d, D/4},
	{NOTE_E4,  2*D},
	{NOTE_G4,  D},

	{NOTE_F4d, D/2+D/4},
	{NOTE_G4,  D/4},
	{NOTE_A4,  2*D},
	{NOTE_B3,  D},

	{NOTE_B4,  2*D},
	{NOTE_C5,  D},
	{NOTE_D4,  D},

	{NOTE_D4,  D/2+D/4},
	{NOTE_E4,  D/4},
	{NOTE_D4,  2*D},
	{NOTE_B4,  D},

	{NOTE_B4,  D/2+D/4},
	{NOTE_C5,  D/4},
	{NOTE_B4,  2*D},
	{NOTE_B4,  D},


	{NOTE_D5,  D/2+D/4},
	{NOTE_C5,  D/4},
	{NOTE_B4,  D},
	{NOTE_B4,  D/2},
	{NOTE_D5,  D/2},
	{NOTE_A4,  D/2},
	{NOTE_D5,  D/2},

	{NOTE_G4,  2*D},

	{    0.0,  0}
};
#undef D
#elif SONGNUM==1
#define SONG god_save_the_queen
#define D 700
const struct note god_save_the_queen[] = {
	{NOTE_G4,  D},
	{NOTE_G4,  D},
	{NOTE_A4,  D},

	{NOTE_F4d, D+D/2},
	{NOTE_G4,  D/2},
	{NOTE_A4,  D},

	{NOTE_B4,  D},
	{NOTE_B4,  D},
	{NOTE_C5,  D},

	{NOTE_B4,  D+D/2},
	{NOTE_A4,  D/2},
	{NOTE_G4,  D},

	{NOTE_A4,  D},
	{NOTE_G4,  D},
	{NOTE_F4d, D},

	{NOTE_G4,  3*D},

	{NOTE_D5,  D},
	{NOTE_D5,  D},
	{NOTE_D5,  D},

	{NOTE_D5,  D+D/2},
	{NOTE_C5,  D/2},
	{NOTE_B4,  D},

	{NOTE_C5,  D},
	{NOTE_C5,  D},
	{NOTE_C5,  D},

	{NOTE_C5,  D+D/2},
	{NOTE_B4,  D/2},
	{NOTE_A4,  D},

	{NOTE_B4,  D},
	{NOTE_C5,  D/2},
	{NOTE_B4,  D/2},
	{NOTE_A4,  D/2},
	{NOTE_G4,  D/2},

	{NOTE_B4,  D+D/2},
	{NOTE_C5,  D/2},
	{NOTE_D5,  D},

	{NOTE_E5,  D/2},
	{NOTE_C5,  D/2},
	{NOTE_B4,  D},
	{NOTE_A4,  D},

	{NOTE_G4,  2*D},

	{    0.0,  0}
};
#undef D
#elif SONGNUM==2
#define SONG ode_to_joy
#define D 500
const struct note ode_to_joy[] = {
	{NOTE_B4,  D},
	{NOTE_B4,  D},
	{NOTE_C5,  D},
	{NOTE_D5,  D},
	{NOTE_D5,  D},
	{NOTE_C5,  D},
	{NOTE_B4,  D},
	{NOTE_A4,  D},
	{NOTE_G4,  D},
	{NOTE_G4,  D},
	{NOTE_A4,  D},
	{NOTE_B4,  D},
	{NOTE_B4,  D+D/2},
	{NOTE_A4,  D/2},
	{NOTE_A4,  2*D},

	{NOTE_B4,  D},
	{NOTE_B4,  D},
	{NOTE_C5,  D},
	{NOTE_D5,  D},
	{NOTE_D5,  D},
	{NOTE_C5,  D},
	{NOTE_B4,  D},
	{NOTE_A4,  D},
	{NOTE_G4,  D},
	{NOTE_G4,  D},
	{NOTE_A4,  D},
	{NOTE_B4,  D},
	{NOTE_A4,  D+D/2},
	{NOTE_G4,  D/2},
	{NOTE_G4,  2*D},

	{NOTE_A4,  D},
	{NOTE_A4,  D},
	{NOTE_B4,  D},
	{NOTE_G4,  D},
	{NOTE_A4,  D},
	{NOTE_B4,  D/2},
	{NOTE_C5,  D/2},
	{NOTE_B4,  D},
	{NOTE_G4,  D},
	{NOTE_A4,  D},
	{NOTE_B4,  D/2},
	{NOTE_C5,  D/2},
	{NOTE_B4,  D},
	{NOTE_A4,  D},
	{NOTE_G4,  D},
	{NOTE_A4,  D},
	{NOTE_D4,  2*D},

	{NOTE_B4,  D},
	{NOTE_B4,  D},
	{NOTE_C5,  D},
	{NOTE_D5,  D},
	{NOTE_D5,  D},
	{NOTE_C5,  D},
	{NOTE_B4,  D},
	{NOTE_A4,  D},
	{NOTE_G4,  D},
	{NOTE_G4,  D},
	{NOTE_A4,  D},
	{NOTE_B4,  D},
	{NOTE_A4,  D+D/2},
	{NOTE_G4,  D/2},
	{NOTE_G4,  2*D},

	{    0.0,  0},
};
#undef D
#elif SONGNUM==3
#define SONG nokia_ringtone
#define D 500
/* http://en.wikipedia.org/wiki/Nokia_tune */
const struct note nokia_ringtone[] = {
	{NOTE_E6,  D/2},
	{NOTE_D6,  D/2},
	{NOTE_F5d, D},
	{NOTE_G5d, D},
	{NOTE_C6d, D/2},
	{NOTE_B5,  D/2},
	{NOTE_D5,  D},
	{NOTE_E5,  D},
	{NOTE_B5,  D/2},
	{NOTE_A5,  D/2},
	{NOTE_C5d, D},
	{NOTE_E5,  D},
	{NOTE_A5,  2*D+D},
	{    0.0,  0}
};
#undef D
#elif SONGNUM==4
#define SONG over_the_rainbow
#define D 600
const struct note over_the_rainbow[] = {
	{NOTE_C4,  2*D},
	{NOTE_C5,  2*D},

	{NOTE_B4,  D},
	{NOTE_G4,  D/2},
	{NOTE_A4,  D/2},
	{NOTE_B4,  D},
	{NOTE_C5,  D},

	{NOTE_C4,  2*D},
	{NOTE_A4,  2*D},

	{NOTE_G4,  4*D},

	{NOTE_A3,  2*D},
	{NOTE_F4,  2*D},

	{NOTE_E4,  D},
	{NOTE_C4,  D/2},
	{NOTE_D4,  D/2},
	{NOTE_E4,  D},
	{NOTE_F4,  D},

	{NOTE_D4,  D},
	{NOTE_B3,  D/2},
	{NOTE_C4,  D/2},
	{NOTE_D4,  D},
	{NOTE_E4,  D},

	{NOTE_C4,  4*D},

	{    0.0,  0}
};
#undef D
#endif

#endif

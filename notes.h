/*
 * Copyright (C) 2013, 2014 Giorgio Vazzana
 *
 * This file is part of Seren.
 *
 * Seren is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Seren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NOTES_H
#define NOTES_H

/* Note frequencies: http://cs.nyu.edu/courses/fall03/V22.0201-003/notes.htm */

#define NOTE_C3  130.81
#define NOTE_C3d 138.59
#define NOTE_D3  146.83
#define NOTE_D3d 155.56
#define NOTE_E3  164.81
#define NOTE_F3  174.61
#define NOTE_F3d 185.00
#define NOTE_G3  196.00
#define NOTE_G3d 207.65
#define NOTE_A3  220.00
#define NOTE_A3d 233.08
#define NOTE_B3  246.94

#define NOTE_C4  261.63
#define NOTE_C4d 277.18
#define NOTE_D4  293.66
#define NOTE_D4d 311.13
#define NOTE_E4  329.63
#define NOTE_F4  349.23
#define NOTE_F4d 369.99
#define NOTE_G4  392.00
#define NOTE_G4d 415.30
#define NOTE_A4  440.00
#define NOTE_A4d 466.16
#define NOTE_B4  493.88

#define NOTE_C5  523.25
#define NOTE_C5d 554.37
#define NOTE_D5  587.33
#define NOTE_D5d 622.25
#define NOTE_E5  659.26
#define NOTE_F5  698.46
#define NOTE_F5d 739.99
#define NOTE_G5  783.99
#define NOTE_G5d 830.61
#define NOTE_A5  880.00
#define NOTE_A5d 932.33
#define NOTE_B5  987.77

#define NOTE_C6  1046.50
#define NOTE_C6d 1108.73
#define NOTE_D6  1174.66
#define NOTE_D6d 1244.51
#define NOTE_E6  1318.51
#define NOTE_F6  1396.91
#define NOTE_F6d 1479.98
#define NOTE_G6  1567.98
#define NOTE_G6d 1661.22
#define NOTE_A6  1760.00
#define NOTE_A6d 1864.66
#define NOTE_B6  1975.53

#endif

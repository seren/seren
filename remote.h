/*
 * Copyright (C) 2013, 2014 Giorgio Vazzana
 *
 * This file is part of Seren.
 *
 * Seren is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Seren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REMOTE_H
#define REMOTE_H

#include <stddef.h>
#include <poll.h>
#include <sys/types.h>

struct pc_remote {
	int     tcpsockfd;
	nfds_t  pi_tcpsocket;
	short   events;

	char    readbuf[4096];
	size_t  readbuf_filled;

	char    writebuf[4096];
	size_t  writebuf_filled;

	int    *verbose;
};

#define PC_REMOTE_CMD_OK             0
#define PC_REMOTE_CMD_ARGERROR      -1
//#define PC_REMOTE_CMD_UNKNOWN       -2
//#define PC_REMOTE_CMD_MISSING_PARAM -3
//#define PC_REMOTE_CMD_INVALID_RANGE -4

enum pc_remote_cmd_id {
	id_unknown = -1,
	id_hangup,
	id_accept,
	id_refuse,
	id_mute,
	id_loop,
	id_autoaccept,
	id_micgain,
	id_bitrate,
	id_mode,
	id_algo,
	id_kill,
	id_nodegain,
	id_call
};

struct pc_remote_cmd {
	enum pc_remote_cmd_id  id;
	size_t                 namelen;
	const char            *name;
};

extern const struct pc_remote_cmd rcommands[];

extern int    pc_remote_arg_int;
extern float  pc_remote_arg_float;
extern char  *pc_remote_arg_string;

void    remote_init(struct pc_remote *remote, int *verbose);
int     remote_open(struct pc_remote *remote, int listening_sockfd);
void    remote_close(struct pc_remote *remote);

ssize_t remote_read_socket(struct pc_remote *remote);
int     remote_extract_line(struct pc_remote *remote, char *buf, size_t len);
int     remote_parse_command(char *line, unsigned int *cmd_idx, int *nb_args_read);

int     remote_append_writebuf(struct pc_remote *remote, char *buf, size_t len);
ssize_t remote_write_socket(struct pc_remote *remote);

#endif

/*
 * Copyright (C) 2013, 2014 Giorgio Vazzana
 *
 * This file is part of Seren.
 *
 * Seren is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Seren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 * Header for the audio dsp module.
 * This module contains audio dsp functions. All of these functions work with
 * stereo vectors (samples must be even).
 */

#ifndef ADSP_H
#define ADSP_H

#include <stddef.h>
#include <stdint.h>

/**
 * Sums a int16_t vector to a int32_t vector.
 * @param[out] pcm32 pointer to the destination vector
 * @param[in]  pcm16 pointer to the source vector
 * @param[in]  samples number of samples, must be even
 */
void     adsp_sum_s32_s16(int32_t *pcm32, const int16_t *pcm16, size_t samples);

/**
 * Copies a int32_t vector to a int16_t vector.
 * @param[out] pcm16 pointer to the destination vector
 * @param[in]  pcm32 pointer to the source vector
 * @param[in]  samples number of samples, must be even
 */
void     adsp_copy_s16_s32(int16_t *pcm16, const int32_t *pcm32, size_t samples);

/**
 * Compresses a int32_t vector to a int16_t vector using hyperbolic tangent function.
 * Intermediate operations are performed using float variables.
 * @param[out] pcm16 pointer to the destination vector
 * @param[in]  pcm32 pointer to the source vector
 * @param[in]  samples number of samples, must be even
 */
void     adsp_compress_tanh_s16_s32(int16_t *pcm16, const int32_t *pcm32, size_t samples);

/**
 * Scales a int32_t vector to a int16_t vector using a fixed gain.
 * Intermediate operations are performed using float variables.
 * @param[out] pcm16 pointer to the destination vector
 * @param[in]  pcm32 pointer to the source vector
 * @param[in]  samples number of samples, must be even
 * @param[in]  gain scale gain
 */
void     adsp_scale_s16_s32(int16_t *pcm16, const int32_t *pcm32, size_t samples, float gain);

/**
 * Sums a int16_t vector to a int16_t vector using clipping.
 * Intermediate operations are performed using int32_t variables, and the
 * result is clipped to the [INT16_MIN, INT16_MAX] range.
 * @param[out] pcm16_d pointer to the destination vector
 * @param[in]  pcm16_s pointer to the source vector
 * @param[in]  samples number of samples, must be even
 */
void     adsp_sum_and_clip_s16_s16(int16_t *pcm16_d, const int16_t *pcm16_s, size_t samples);

/**
 * Scales a int16_t vector using a fixed gain and clipping.
 * Intermediate operations are performed using float and int32_t variables,
 * and the result is clipped to the [INT16_MIN, INT16_MAX] range.
 * @param[out] pcm16 pointer to the destination vector
 * @param[in]  samples number of samples, must be even
 * @param[in]  gain scale gain
 */
void     adsp_scale_and_clip_s16_s16(int16_t *pcm16, size_t samples, float gain);

/**
 * Finds the peak in a 2-channel int16_t vector.
 * @param[in] pcm pointer to the vector
 * @param[in] samples number of samples, must be even
 * @return    the peak
 */
int32_t  adsp_find_peak_s16_2ch(const int16_t *pcm, size_t samples);

/**
 * Finds the peak in a 2-channel int32_t vector.
 * @param[in] pcm pointer to the vector
 * @param[in] samples number of samples, must be even
 * @return    the peak
 */
int32_t  adsp_find_peak_s32_2ch(const int32_t *pcm, size_t samples);

/**
 * Computes the sum of squares in a int16_t vector.
 * @param[in] pcm pointer to the vector
 * @param[in] samples number of samples, must be even
 * @return    sum of squares
 */
uint64_t adsp_sum_of_squares(const int16_t *pcm, size_t samples);

#endif
